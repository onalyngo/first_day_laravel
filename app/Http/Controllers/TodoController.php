<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Todo; //to access Todo model
use \App\Category; //to access Category model

class TodoController extends Controller
{
    public function index(){

    	$tasks = Todo::all();  //SELECT * FROM todo in php
    	// dd($tasks); //dump and die like var_dump in php

    	return view('tasks', compact('tasks'));
    }

    public function create(){

    	$categories = Category::all();

    	return view('add-task', compact('categories'));
    }

    public function store(Request $req){
    	// dd($req);

    	$new_task = new Todo;
    	$new_task->title = $req->title;
    	$new_task->body = $req->body;
    	$new_task->category_id = $req->category_id;
    	$new_task->status_id = 1;
    	$new_task->user_id = 1;

    	$new_task->save();

    	return redirect('/tasks');
    }

    public function destroy($id)
    {//find the data to delete
    	$taskToDelete = Todo::find($id);
    	// dd($taskToDelete);
    	$taskToDelete->delete();
    	return redirect('/tasks');
    }

    public function markAsDone($id){
    	//Find the task to update
    	$taskToUpdate = Todo::find($id);

    	if($taskToUpdate->status_id == 3){
    		$taskToUpdate->status_id = 1;
    	}else{
    		$taskToUpdate->status_id = 3;
    	}

    	// $taskToUpdate->status_id = 3;
    	$taskToUpdate->save();
    	return redirect('/tasks');

    }
}
