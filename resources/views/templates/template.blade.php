<html>
<head>
	<meta charset="UTF-8">
	<title>@yield("title")</title>

	{{-- bootswatch --}}
	<link rel="stylesheet" href="https://bootswatch.com/4/darkly/bootstrap.css">
</head>
<body>
	
	{{-- navbar bootswatch --}}
	<nav class="navbar navbar-expand-lg navbar-light bg-success">
	  <a class="navbar-brand" href="#">Navbar</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarColor01">
	    <ul class="navbar-nav mr-auto">
	      <li class="nav-item active">
	        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#">Features</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#">Pricing</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#">About</a>
	      </li>
	    </ul>
	  </div>
	</nav>
	
	@yield("content")

	<footer class="footer bg-success">
		<div class="container">
			<p class="text-center text-dark">&copy; 2020 Made By: Onalyn Go </p>
		</div>
		
	</footer>

</body>
</html>