<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/profile', function () {
    return view('profile');
});


// Route::get('/welcome', function () {
//     return view('welcome');
// });

Route::get('/welcome', 'Controller@index');


Route::get('/blade1', function () {
    return view('blade1');
});


Route::get('/blade2', function () {
    return view('blade2');
});


Route::get('/blade3', function () {
    return view('blade3');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//To show all tasks
Route::get('/tasks', 'TodoController@index');

//To show add task form
Route::get('/addtask', 'TodoController@create');

//To save the new Task
Route::post('/addtask', 'TodoController@store');

//To delete task
Route::delete('/deletetask/{id}', 'TodoController@destroy');

//To mark as done
Route::patch('/markasdone/{id}', 'TodoController@markAsDone');